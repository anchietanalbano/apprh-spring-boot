package com.AppRh.AppRH.controllers;

import com.AppRh.AppRH.repository.DependentesRepository;
import com.AppRh.AppRH.repository.FuncionarioRepository;
import jakarta.persistence.Table;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.AppRh.AppRH.models.Dependentes;
import com.AppRh.AppRH.models.Funcionario;

@Controller
@Table(name = "funcionario")
public class FuncionarioController {

    @Autowired
    private FuncionarioRepository fr;
    @Autowired
    private DependentesRepository dr;

    @RequestMapping(value = "/cadastrarFuncionario", method = RequestMethod.GET)
    public String form() {
        return "funcionario/formFuncionario";
    }

    // Cadastrar funcionario
    @RequestMapping(value = "/cadastrarFuncionario", method = RequestMethod.POST)
    public String form(@Valid Funcionario funcionario, BindingResult result, RedirectAttributes attributes) {
        if (result.hasErrors()) {
            attributes.addFlashAttribute("mensagem", "Verifique os campos.");
            return "redirect:/cadastrarFuncionario";
        }
        fr.save(funcionario);
        attributes.addFlashAttribute("mensagem", "Funcionario cadastrado com sucesso!");
        return "redirect:/cadastrarFuncionario";
    }

    //Listar funcionário
    @RequestMapping("/funcionarios")
    public ModelAndView listaFuncionarios() {
        ModelAndView mv = new ModelAndView("funcionario/listaFuncionario");
        Iterable<Funcionario> funcionarios = fr.findAll();
        mv.addObject("funcionarios", funcionarios);
        return mv;
    }

    // Listar dependentes
    @RequestMapping(value = "/dependentes/{id}", method = RequestMethod.GET)
    public ModelAndView dependentes(@PathVariable(value = "id") long id) {
        Funcionario funcionario = fr.findById(id);
        ModelAndView mv = new ModelAndView("funcionario/dependentes");
        mv.addObject("funcionarios", funcionario);

        // lista de dependentes baseada no funcionario
        Iterable<Dependentes> dependentes = dr.findByFuncionario(funcionario);
        mv.addObject("dependentes", dependentes);

        return mv;
    }

    // Adicionar Dependentes
    @RequestMapping(value = "/dependentes/{id}", method = RequestMethod.POST)
    public String dependentesPost(@PathVariable("id") long id, Dependentes dependente, BindingResult result, RedirectAttributes attributes) {

        if (result.hasErrors()) {
            attributes.addFlashAttribute("mensagem", "Verifique os campos!");
            return "redirect:/dependentes/{id}";
        }
        if (dr.findByCpf(dependente.getCpf()) != null) {
            attributes.addFlashAttribute("mensagem erro", "CPF duplicado");
            return "redirect:/dependentes/{id}";
        }
        Funcionario funcionario = fr.findById(id);
        dependente.setFuncionario(funcionario);
        dr.save(dependente);
        attributes.addFlashAttribute("mensagem", "Dependente adicionado com sucesso!");
        return "redirect:/dependentes/{id}";
    }

    // deletar funcionario
    @RequestMapping("/deletarFuncionario")
    public String deletarFuncionario(long id) {
        Funcionario funcionario = fr.findById(id);
        fr.delete(funcionario);
        return "redirect:/funcionarios";
    }

    // Métodos que atualiza funcionário
    // form
    @RequestMapping(value = "/editarFuncionario", method = RequestMethod.GET)
    public ModelAndView editarFuncionario(long id) {
        Funcionario funcionario = fr.findById(id);
        ModelAndView mv = new ModelAndView("funcionario/update-funcionario");
        mv.addObject("funcionario", funcionario);
        return mv;
    }

    //update funcionario
    @RequestMapping(value = "/editarFuncionario", method = RequestMethod.POST)
    public String updateFuncionario(@Valid Funcionario funcionario, BindingResult result, RedirectAttributes attributes) {

        fr.save(funcionario);
        attributes.addFlashAttribute("success", "Funcionário alterado com sucesso!");

        long idLong = funcionario.getId();
        String id = "" + idLong;
        return "redirect:/dependentes/" + id;
    }

    // deletar dependente
    @RequestMapping("/deletarDependente")
    public String deletarDependente(String cpf){
        Dependentes dependente = dr.findByCpf(cpf);

        Funcionario funcionario = dependente.getFuncionario();
        String codig = "" + funcionario.getId();
        dr.delete(dependente);

        return "redirect:/dependentes/" + codig;
    }


}
