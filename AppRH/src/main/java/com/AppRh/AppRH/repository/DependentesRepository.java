package com.AppRh.AppRH.repository;

import com.AppRh.AppRH.models.Candidato;
import com.AppRh.AppRH.models.Dependentes;
import com.AppRh.AppRH.models.Funcionario;
import com.AppRh.AppRH.models.Vaga;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


import java.util.List;

public interface DependentesRepository extends CrudRepository<Dependentes, String> {

    Iterable<Dependentes> findByFuncionario(Funcionario funcionario);

    // para metodo deletar
    Dependentes findByCpf(String cpf);

    Dependentes findById(long id);

    //List<Dependentes> findByNome(String nome);
    @Query(value = "select u from Dependentes u where u.nome like %?1%")
    List<Dependentes> findByNomesDependentes(String nome);

}
