package com.AppRh.AppRH.repository;

import java.util.List;

import com.AppRh.AppRH.models.Funcionario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.AppRh.AppRH.models.Vaga;

// Normalmente os sistemas têm sempre um Crud implantado
// Create, read, update e delete
public interface VagaRepository extends CrudRepository<Vaga,String> {

    // Métodos para trabalhar com pesquisa no nosso repositorio
    Vaga findByCodigo(long codigo);
    List<Vaga> findByNome(String nome);

    @Query(value = "select u from Vaga u where u.nome like %?1%")
    List<Vaga> findByNomesVaga(String nome);

}
