package com.AppRh.AppRH.repository;


import com.AppRh.AppRH.models.Candidato;
import com.AppRh.AppRH.models.Vaga;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CandidatoRepository extends CrudRepository<Candidato, String> {

    Iterable<Candidato> findByVaga(Vaga vaga);

    Candidato findByRg(String rg);

    Candidato findById(long id);

    //List<Candidato>findByNomeCandidato(String nome);

    @Query(value = "select u from Candidato u where u.nomeCandidato like %?1%")
    List<Candidato> findByNomesCandidatos(String nome);
}
