# Aplicação de Recursos Humanos (RH) - API

Esta é uma API para gerenciar recursos humanos, com foco em operações CRUD (Create, Read, Update, Delete). A aplicação
permite que os usuários gerenciem informações relacionadas aos funcionários, candidatos e outras entidades relevantes.


## Arquitetura
![Alt text](img/img.png)

## Endpoints

A API oferece os seguintes endpoints:

1. **Vagas**:
   - Listar todos as vagas
   - Recuperar detalhes de uma vaga por ID
   - Criar uma vaga
   - Atualizar informações de uma vaga existente
   - Excluir uma vaga
   - Adicionar candidato
   - Deletar candidato por RG 

## Tecnologias

- [Spring Boot](https://spring.io/projects/spring-boot)
- [Spring MVC](https://docs.spring.io/spring-framework/reference/web/webmvc.html)
- [Spring Data JPA](https://spring.io/projects/spring-data-jpa)
- [Mysql](https://dev.mysql.com/downloads/)


## Práticas Adotadas

- SOLID, DRY, YAGNI
- API REST
- Consultas com Spring Data JPA
- Injeção de Dependências
- Tratamento de Respostas de Erro

## Configuração

- Configure o banco de dados no arquivo `application.properties`.
- Execute o aplicativo Spring Boot usando `mvn spring-boot:run`.
 
